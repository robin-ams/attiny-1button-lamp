#include <FastLED.h>

#define LED_PIN     0
#define NUM_LEDS    12
#define BRIGHTNESS  230
#define LED_TYPE    WS2812
#define COLOR_ORDER GRB

CRGB leds[NUM_LEDS];
CRGBPalette16 currentPalette;
TBlendType    currentBlending = LINEARBLEND;

extern const TProgmemPalette16 CustomPalette_p PROGMEM;
extern const TProgmemPalette16 CustomPalette2_p PROGMEM;

const uint8_t buttonPin = 3;

uint8_t mode = 0;
uint8_t buttonState;
boolean lastButtonState = LOW;
long lastDebounceTime = 0;
uint8_t debounceDelay = 20;
boolean buttonPressed = false;

int brightness = BRIGHTNESS;
int stripbrightness = BRIGHTNESS;
int pstripbrightness;

boolean fading = false;

long prevmillis;
long prevmillis2;
long prevmillis3;
uint8_t switchpalette = 0;
uint8_t startIndex = 0;

void setup() {

    delay( 1000 );
    FastLED.addLeds<LED_TYPE, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS).setCorrection( TypicalLEDStrip );
    FastLED.setBrightness(  BRIGHTNESS );

    pinMode(buttonPin, INPUT);
}

void loop() {

    FastLED.show();

    if (mode > 3) { mode = 0; }

    if (mode == 0) {
        setWarWhitePalette();
        FillLEDsFromPaletteColors(startIndex, 30, 10);
    }
    else if (mode == 1) {
        rainbowCycle();
    }
    else if (mode == 2) {
        ChangePalettePeriodically();
        FillLEDsFromPaletteColors(startIndex, 120, 6);
    }
    else if (mode == 3) {
        ChangePalettePeriodically();
        FillLEDsFromPaletteColors(startIndex, 8, 30);
    }

    fadeIn();
    interupt();
}

uint8_t hueIndex = 0;

void rainbowCycle() {

    if(millis() > prevmillis2 + 500){

        for (int c = 0; c < NUM_LEDS; c++) {

            if(hueIndex > 255) {
                hueIndex = 0;    
            }
            leds[c] = CHSV(hueIndex,255,255);
            interupt();
        }
        hueIndex++;
        prevmillis2 = millis();
    }
}

void FillLEDsFromPaletteColors( uint8_t colorIndex, int interval, uint8_t spread) {

    if(millis() > prevmillis3 + interval){
        startIndex = startIndex + 1;
    
        for ( int i = 0; i < NUM_LEDS; i++) {
            leds[i] = ColorFromPalette( currentPalette, colorIndex, brightness, currentBlending);
            colorIndex += spread;
            interupt();
            prevmillis3 = millis();
        }
    }
}


boolean doFadeAndSwitch = false;
boolean fadeOutDone = false;
boolean fadeInDone = true;
unsigned int interval;

void ChangePalettePeriodically() {


    uint8_t timeUnit = 0;

    if (mode == 3) {
        interval = 10000;
    }
    else {
        interval = 30000;
    }

    if(millis() > prevmillis2+interval) {
        doFadeAndSwitch = true;
        prevmillis2 = millis();
    }
    if(switchpalette == 5) { switchpalette = 0; }
    
    if(switchpalette == 0) {
        currentPalette = CustomPalette2_p;
    }
    if(switchpalette == 1) {
        currentPalette = CustomPalette_p;
    }
    if(switchpalette == 2) {
        currentPalette = OceanColors_p;
    }
    if(switchpalette == 3) {
        currentPalette = ForestColors_p;
    }
    if(switchpalette == 4) {
        currentPalette = LavaColors_p;
    }
    if(switchpalette == 5) {
        currentPalette = PartyColors_p;
    }
    if(switchpalette == 6) {
        currentPalette = RainbowColors_p;
    }

    fadeAndSwitch();
}

void fadeAndSwitch() {

    if (!doFadeAndSwitch) {
        return;
    }
    if (millis() > prevmillis + 10 && !fadeOutDone) {
        stripbrightness--;
        prevmillis = millis();

        if (stripbrightness <= 0) {
            fadeOutDone = true;
            fadeInDone = false;
            switchpalette++;;
        }
    }
    else if (millis() > prevmillis + 10 && !fadeInDone) {
        stripbrightness++;
        prevmillis = millis();
    }
    if (stripbrightness >= BRIGHTNESS) {
        fadeInDone = true;
        fadeOutDone = false;
        doFadeAndSwitch = false;
    }
    FastLED.setBrightness(stripbrightness);
}


void fadeIn() {

    if (!fading) {
        pstripbrightness = stripbrightness;
        return;
    }
    if (stripbrightness < pstripbrightness && millis() > prevmillis + 10) {
        stripbrightness++;
        prevmillis = millis();
    }
    if (stripbrightness >= pstripbrightness) {
        fading = false;
        stripbrightness = pstripbrightness;
    }
    FastLED.setBrightness(stripbrightness);
}

boolean interupt() {

    int reading = digitalRead(buttonPin);

    if (reading != lastButtonState) {
        lastDebounceTime = millis();
    }
    if ((millis() - lastDebounceTime) > debounceDelay) {

        if (reading != buttonState) {
            buttonState = reading;

            if (buttonState == HIGH) {
                buttonPressed = true;
                return false;
            }

            if (buttonState == LOW && buttonPressed) {

                stripbrightness = 0;
                fading = true;
                mode++;
                buttonPressed = false;
                return true;
            }
        }
    }
    lastButtonState = reading;
}

const TProgmemPalette16 CustomPalette_p PROGMEM =
{

    CRGB::Orange,
    CRGB::Orange,
    CRGB::Orange,
    CRGB::Green,
    CRGB::Olive,
    CRGB::Olive,
    CRGB::Orange,
    CRGB::Orange,
    CRGB::Maroon,
    CRGB::Maroon,
    CRGB::Maroon,
    CRGB::Red,
    CRGB::OrangeRed,
    CRGB::Orange,
    CRGB::Maroon,
    CRGB::Red
};

const TProgmemPalette16 CustomPalette2_p PROGMEM =
{

    CRGB::Orange,
    CRGB::Blue,
    CRGB::Orange,
    CRGB::OrangeRed,
    CRGB::Orange,
    CRGB::OrangeRed,
    CRGB::OliveDrab,
    CRGB::Yellow,
    CRGB::Maroon,
    CRGB::Maroon,
    CRGB::OrangeRed,
    CRGB::SeaGreen,
    CRGB::Orange,
    CRGB::OrangeRed,
    CRGB::Maroon,
    CRGB::Yellow
};


void setWarWhitePalette(){
  
  CRGB warm   = CRGB(240, 120, 0);
  CRGB loWarm = CRGB(120, 60, 0);
  CRGB hiWarm   = CRGB(255, 170, 10);

  currentPalette = CRGBPalette16(
    hiWarm, hiWarm, warm, warm,
    warm, loWarm, loWarm, hiWarm,
    warm, warm, hiWarm, hiWarm,
    warm, warm, loWarm, loWarm);
}

